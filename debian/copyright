Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: horizon
Source: https://github.com/openstack/horizon

Files: *
Copyright: (c) 2011-2017, OpenStack Foundation
           (c) 2011, Nebula, Inc
           (c) 2011, United States Government as represented by the Administrator
                of the National Aeronautics and Space Administration
           (c) 2012-2013, NTT MCL, Inc.
           (c), 2012 X.commerce, a business unit of eBay Inc.
           (c) 2015-2016, Hewlett Packard Enterprise Software, LLC
           (c) 2013-2016, Hewlett-Packard Development Company, L.P.
           (c) 2014-2017, Rackspace US, Inc
           (c) 2015-2017, Cisco Systems, Inc.
           (c) 2015, Thales Services SAS
           (c) 2012-2018, NEC Corporation
           (c) 2015, Alcatel-Lucent USA Inc.
           (c) 2014-2016, IBM Corp.
           (c) 2015, Yahoo! Inc. All Rights Reserved.
           (c) 2017, Ericsson
           (c) 2012-2017, Red Hat, Inc.
           (c) 2015-2016, Mirantis Inc.
           (c) 2015, ThoughtWorks Inc.
           (c) 2013, CentRin Data, Inc.
           (c) 2013, Big Switch Networks, Inc.
           (c) 2016, Symantec Corp.
           (c) 2016, 99Cloud
           (c) 2016, Letv Cloud Computing
           (c) 2014, Kylincloud
           (c) 2014-2015, Intel Corp.
           (c) 2013, B1 Systems GmbH
           (c) 2013, Centrin Data Systems Ltd.
           (c) 2013, Metacloud, Inc.
           (c) 2017, SUSE LLC
           (c) 2011-2013, Canonical Ltd.
           (c) 2012, CRS4
License: Apache-2.0

Files: debian/*
Copyright: (c) 2011, Julien Danjou <acid@debian.org>
           (c) 2012-2018, Thomas Goirand <zigo@debian.org>
           (c) 2018-2019, Michal Arbet <michal.arbet@ultimum.io>
License: Apache-2.0

License: Apache-2.0
 Licensed under the Apache License, Version 2.0 (the "License"); you may not
 use this file except in compliance with the License. You may obtain a copy of
 the License at
 .
 http://www.apache.org/licenses/LICENSE-2.0
 .
 Unless required by applicable law or agreed to in writing, software
 distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 License for the specific language governing permissions and limitations under
 the License.
 .
 On Debian systems, the complete text of the Apache version 2.0 license can be
 found in "/usr/share/common-licenses/Apache-2.0".
