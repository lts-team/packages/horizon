#!/usr/bin/make -f

include /usr/share/openstack-pkg-tools/pkgos.make

%:
	dh $@ --buildsystem=python_distutils --with python3,sphinxdoc

override_dh_install:
	echo "Do nothing..."

override_dh_auto_clean:
	python3 setup.py clean
	rm -rf build
	rm -rf $(CURDIR)/static
	rm -rf $(CURDIR)/horizon/build
	rm -rf $(CURDIR)/horizon.egg-info
	rm -rf $(CURDIR)/openstack_dashboard/openstack_dashboard.egg-info
	rm -rf doc/source/sourcecode
	rm -rf $(CURDIR)/doc/build
	rm -f horizon/openstack_dashboard/test/.secret_key_store
	find $(CURDIR)/openstack_dashboard -iname '*.pyc' -delete
	rm -rf .eggs
	rm -f AUTHORS
	rm -f ChangeLog

override_dh_auto_build:
	echo "Do nothing..."

override_dh_auto_install:

	# Compile language files
	chmod +x debian/compile-messages
	$(CURDIR)/debian/compile-messages

	set -e && for pyvers in $(PYTHON3S); do \
		python$$pyvers setup.py install \
			--install-layout=deb \
			--root $(CURDIR)/debian/tmp \
			--no-compile -O0; \
	done

	# Prepare openstack-dashboard folder and copy manage.py
	install -d -m 755 $(CURDIR)/debian/tmp/usr/share/openstack-dashboard
	cp $(CURDIR)/manage.py $(CURDIR)/debian/tmp/usr/share/openstack-dashboard/

	# Move themes folder to standard location
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/themes \
		$(CURDIR)/debian/tmp/usr/share/openstack-dashboard/

	# Create /etc/openstack-dashboard/ and move policy files into
	install -d -m 755 $(CURDIR)/debian/tmp/etc/openstack-dashboard/
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/conf \
		$(CURDIR)/debian/tmp/etc/openstack-dashboard/policy

	# Move conf to standard location
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/enabled \
		$(CURDIR)/debian/tmp/etc/openstack-dashboard/
	rm -f $(CURDIR)/debian/tmp/etc/openstack-dashboard/enabled/__init__.py
	rm -f $(CURDIR)/debian/tmp/etc/openstack-dashboard/enabled/*.example

	# Move wsgi file to standard location
	mv $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/wsgi.py \
		$(CURDIR)/debian/tmp/usr/share/openstack-dashboard/

	# Move local_settings.d to /etc and modify local_settings.py
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.d \
		$(CURDIR)/debian/tmp/etc/openstack-dashboard/
	rm -f $(CURDIR)/debian/tmp/etc/openstack-dashboard/local_settings.d/*.example

	install -d -m 755 $(CURDIR)/debian/tmp/usr/share/openstack-dashboard/examples
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/local_settings.py.example \
		$(CURDIR)/debian/tmp/usr/share/openstack-dashboard/examples/local_settings.py

	# Secret key has to be replaced here
	sed -i -e 's/os.path.join(LOCAL_PATH,/os.path.join("\/","var","lib","openstack-dashboard","secret-key",/' $(CURDIR)/debian/tmp/usr/share/openstack-dashboard/examples/local_settings.py

	# Move osprofiler conf files to be enabled to standard location
	# and remove /usr/lib/python3/dist-packages/openstack_dashboard/contrib/developer/enabled
	mv -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/contrib/developer/enabled/_[0-9]*.py \
		$(CURDIR)/debian/tmp/etc/openstack-dashboard/enabled/
	rm -rf $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/contrib/developer/enabled

	# Remove secret_key_store as we will use it in /var/lib/
	rm -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/.secret_key_store

	## Delete __pycache__
	find . -name __pycache__ -prune -exec rm -rf {} +;

	## Delete not needed files
	rm -f $(CURDIR)/debian/tmp/usr/lib/python3/dist-packages/openstack_dashboard/local/_build*.lock

	dh_install
	dh_missing --fail-missing
	find $(CURDIR)/debian -iname .eslintrc -delete

override_dh_sphinxdoc:
ifeq (,$(findstring nodocs, $(DEB_BUILD_OPTIONS)))
	python3 -m sphinx -b html doc/source debian/horizon-doc/usr/share/doc/horizon-doc/html
	dh_sphinxdoc -O--buildsystem=python_distutils
endif

override_dh_auto_test:
ifeq (,$(findstring nocheck, $(DEB_BUILD_OPTIONS)))
	# We should add --compilemessages when the .mo are removed.
	set -e ; set -x ; for i in $(PYTHON3S) ; do \
		http_proxy=127.0.0.1:9 https_proxy=127.0.0.9:9 \
		HTTP_PROXY=127.0.0.1:9 HTTPS_PROXY=127.0.0.1:9 \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i -m coverage run $(CURDIR)/manage.py test horizon --verbosity 2 --settings=horizon.test.settings --exclude-tag selenium --exclude-tag integration; \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i -m coverage run $(CURDIR)/manage.py test openstack_dashboard --verbosity 2 --settings=openstack_dashboard.test.settings --exclude-tag selenium --exclude-tag integration; \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i -m coverage run $(CURDIR)/manage.py test openstack_auth --verbosity 2 --settings=openstack_auth.tests.settings --exclude-tag selenium --exclude-tag integration; \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i $(CURDIR)/manage.py test horizon --settings=horizon.test.settings --verbosity 2 --exclude-tag selenium --exclude-tag integration ; \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i $(CURDIR)/manage.py test openstack_dashboard --settings=openstack_dashboard.test.settings --verbosity 2 --exclude-tag selenium --exclude-tag integration ; \
		PYTHONPATH=$(CURDIR)/debian/tmp/usr/lib/python3/dist-packages PYTHON=python$$i python$$i $(CURDIR)/manage.py test openstack_auth --settings=openstack_auth.tests.settings --verbosity 2 --exclude-tag selenium --exclude-tag integration ; \
	done
	## Delete __pycache__
	find . -name __pycache__ -prune -exec rm -rf {} +;
endif
