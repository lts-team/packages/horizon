# Translation of horizon debconf template to Swedish
# Copyright (C) 2015 Martin Bagge <brother@bsnet.se>
# This file is distributed under the same license as the horizon package.
#
# Martin Bagge <brother@bsnet.se>, 2013, 2015
msgid ""
msgstr ""
"Project-Id-Version: horizon\n"
"Report-Msgid-Bugs-To: horizon@packages.debian.org\n"
"POT-Creation-Date: 2018-08-30 11:59+0200\n"
"PO-Revision-Date: 2015-04-01 16:51+0100\n"
"Last-Translator: Martin Bagge / brother <brother@bsnet.se>\n"
"Language-Team: Swedish <debian-l10n-swedish@lists.debian.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.6.10\n"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid "Activate Dashboard and disable default VirtualHost?"
msgstr ""
"Aktivera Dashboard och avaktivera den standardiserade virtuella värden?"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"The Apache package sets up a default web site and a default page, configured "
"in /etc/apache2/sites-available/000-default.conf."
msgstr ""
"Apache-paketet levereras med en standard webbplats och en standard sida, "
"dessa inställningar finns i /etc/apache2/sites-available/000-default.conf."

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"If this option is not selected, Horizon will be installed using /horizon "
"instead of the webroot."
msgstr ""

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:2001
msgid ""
"Choose this option to replace that default with the OpenStack Dashboard "
"configuration."
msgstr ""
"Välj denna väg för att ersätta standard-Dashboard i OpenStack med Horizon."

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:3001
msgid "Should the Dashboard use HTTPS?"
msgstr "Ska Dashboard använda HTTPS?"

#. Type: boolean
#. Description
#: ../openstack-dashboard-apache.templates:3001
msgid ""
"Select this option if you would like Horizon to be served over HTTPS only, "
"with a redirection to HTTPS if HTTP is in use."
msgstr ""
"Välj detta om du vill att Horizon endast ska levereras på HTTPS, en "
"omdirigering av HTTP till HTTPS kommer att aktiveras."

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid "Allowed hostname(s):"
msgstr ""

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Please enter the list of hostnames that can be used to reach your OpenStack "
"Dashboard server. This is a security measure to prevent HTTP Host header "
"attacks, which are possible even under many seemingly-safe web server "
"configurations."
msgstr ""

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Enter values separated by commas. Any space will be removed, so you can add "
"some to make it more readable."
msgstr ""

#. Type: string
#. Description
#: ../openstack-dashboard.templates:2001
msgid ""
"Values in this list can be fully qualified names like \"www.example.com\", "
"in which case they will be matched against the request's Host header exactly "
"(case-insensitive, not including port). A value beginning with a period can "
"be used as a subdomain wildcard: \".example.com\" will match example.com, "
"www.example.com, and any other subdomain of example.com. A value of \"*\" "
"will match anything; in this case you are responsible for providing your own "
"validation of the Host header (perhaps in middleware; if so this middleware "
"must be listed first in MIDDLEWARE)."
msgstr ""
